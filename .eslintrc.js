module.exports = {
  extends: ['airbnb', 'prettier'],
  plugins: ['react-hooks', 'prettier'],
  rules: {
    // 'prettier/prettier': ['warn'],
    'react/jsx-filename-extension': 'off',
    'linebreak-style': 0,
    'import/no-cycle': 0,
    'import/prefer-default-export': 0,
    'react/require-default-props': 0,
    'click-events-have-key-events': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'react/jsx-props-no-spreading': 0,
    'no-undef': 0,
    'jsx-a11y/control-has-associated-label': 0,
    'import/no-dynamic-require': 0,
    'global-require': 0,
    'react/forbid-prop-types': 0,
    'max-len': [2, 200, 4, { ignoreUrls: true }],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'off',
  },
};
