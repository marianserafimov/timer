## Installation

- All the `code` required to get started
- Images of what it should look like

### Clone

- Clone this repo to your local machine using `https://gitlab.com/marianserafimov/timer.git`

### Setup

> install npm packages

```shell
$ npm install
```

> install and run json-server

```shell
$ npm install -g json-server
```

> run json-server

```shell
$ json-server --watch db.json
```
```

> then run the application

```shell
$ npm start
```

### Exercise:

Build a timer with React.js with the following functionalities - start, stop, reset.
Assumptions:

- UI should consist of 3 input fields - (hours minutes and seconds) and 3 buttons (start, stop, reset)
- User should be able to enter the initial value of the timer.
- The UI should be minimalistic and CSS up to your choice.

Additional task:
Create a step input field which will determine the step by which the timer will increment - Value 10 means the
timer will increase 10 times faster.

- Copyright Date.now()© Timer.
