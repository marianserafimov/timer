export const ERROR_MESSAGES = {
  ERROR_401: 'Authorization failed',
  ERROR_403: "You don't have the permissions to execute this operation.",
  ERROR_404: 'The page you are trying to open was not found',
  ERROR_500:
    'Unexpected server error. Please contact your system administrator.',
  ERROR_UNKNOWN:
    'Unknown system error. Please contact your system administrator.',
};
