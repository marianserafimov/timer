import React from 'react';

import * as PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import Header from '../components/common/header/Header';
import Footer from '../components/common/Footer';
import Preloader from '../components/common/Preloader';
import { isPreloaderLoadingSelector } from '../store/selectors';

function MainLayout({ children }) {
  const isPreloaderLoading = useSelector(isPreloaderLoadingSelector);

  return (
    <>
      <Header />
      <div className="content">{children}</div>
      <Footer />
      {isPreloaderLoading && <Preloader />}
    </>
  );
}

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayout;
