import { ERROR_MESSAGES } from '../constants/errorMessages';

const generateError = (error) => {
  let errMessage = '';
  if (error && error.response && error.response.status) {
    switch (error.response.status) {
      case 401:
        errMessage = ERROR_MESSAGES.ERROR_401;
        break;
      case 403:
        errMessage = ERROR_MESSAGES.ERROR_403;
        break;
      case 404:
        errMessage = ERROR_MESSAGES.ERROR_404;
        break;
      case 500:
        errMessage = ERROR_MESSAGES.ERROR_500;
        break;
      default:
        errMessage = JSON.stringify(error.response.data);
        break;
    }

    errMessage = `Error : ${errMessage} (Code: ${error.response.status})`;
  } else {
    errMessage = ERROR_MESSAGES.ERROR_UNKNOWN;
  }
  return errMessage;
};

export default generateError;
