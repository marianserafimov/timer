import { combineEpics } from 'redux-observable';

import apiRequestEpic$ from './apiRequestEpic';
import getWeatherEntriesEpic$ from '../../features/weather/epics/getWeatherEntriesEpic';

const rootEpic = (apis) =>
  combineEpics(apiRequestEpic$(apis), getWeatherEntriesEpic$);

export default rootEpic;
