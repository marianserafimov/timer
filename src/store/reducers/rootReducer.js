import { combineReducers } from 'redux';

import PreloaderReducer from './preloaderReducer';
import NotificationsReducer from './notificationsReducer';
import WeatherReducer from '../../features/weather/reducers/weatherReducer';

const rootReducer = combineReducers({
  preloader: PreloaderReducer,
  weather: WeatherReducer,
  notifications: NotificationsReducer,
});

export default rootReducer;
