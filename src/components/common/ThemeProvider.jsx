import React, { useState, createContext } from 'react';

import * as PropTypes from 'prop-types';

import cfg from '../../cfg';

export const ThemeContext = createContext();

const ThemeProvider = ({ children }) => {
  const { dark, light, defaultTheme } = cfg.texts.themes;
  const [mode, setTheme] = useState(defaultTheme);

  return (
    <ThemeContext.Provider
      value={{
        mode,
        setTheme: () => setTheme(mode === dark ? light : dark),
      }}>
      {children}
    </ThemeContext.Provider>
  );
};

ThemeProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

export default ThemeProvider;
