import React from 'react';

import Switch from 'react-switch';
import * as PropTypes from 'prop-types';

const ToggleSwitch = ({
  checked = true,
  onChange,
  checkedIcon = false,
  uncheckedIcon = false,
  onColor = '#FDB813',
  offColor = '#1d1f2f',
  height = 25,
  width = 60,
}) => (
  <Switch
    checked={checked}
    height={height}
    width={width}
    onColor={onColor}
    offColor={offColor}
    checkedIcon={checkedIcon}
    uncheckedIcon={uncheckedIcon}
    onChange={onChange}
  />
);

ToggleSwitch.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  checkedIcon: PropTypes.element,
  uncheckedIcon: PropTypes.element,
  onColor: PropTypes.string,
  offColor: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
};

export default ToggleSwitch;
