import React from 'react';

import cfg from '../../../cfg';
import Navigation from '../../../features/navigation';
import HeaderSwitch from './components/HeaderSwitch';

function Header() {
  return (
    <>
      <div className="header">
        <h1>{cfg.texts.logo}</h1>
        <Navigation />
        <HeaderSwitch />
      </div>
    </>
  );
}

export default Header;
