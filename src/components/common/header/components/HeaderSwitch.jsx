import React, { useEffect, useContext } from 'react';

import ToggleSwitch from '../../ToggleSwitch';
import { ThemeContext } from '../../ThemeProvider';
import cfg from '../../../../cfg';

function HeaderSwitch() {
  const { light } = cfg.texts.themes;
  const { cssColorVariables } = cfg;

  const { setTheme, mode } = useContext(ThemeContext);
  const isOn = mode === light;

  const onSwitchChange = ()=> {
    setTheme(!mode);
  }

  useEffect(() => {
    cssColorVariables.forEach((varName) => {
      const color = getComputedStyle(document.body).getPropertyValue(
        `--${varName}-${mode}`,
      );
      document.documentElement.style.setProperty(`--${varName}`, color);
    });
  }, [mode]);

  return <ToggleSwitch checked={isOn} onChange={onSwitchChange} />;
}

export default HeaderSwitch;
