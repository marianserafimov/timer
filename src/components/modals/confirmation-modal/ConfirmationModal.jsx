import React from 'react';

import * as PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

import cfg from '../../../cfg';

function ConfirmationModal(props) {
  const onClose = () => {
    props.onClose();
  }

  const onConfirm = () => {
    onClose();
    props.onConfirm();
  }
  const { className, children, btn1, btn2 } = props;
  return (
    <div className={`confirmation-modal ${className || ''}`} onClick={onClose}>
      <div
        className="confirmation-modal-container"
        onClick={(event) => event.stopPropagation()}>
        {children}
        <div className="modal-buttons">
          {btn1 && (
            <Button type={cfg.buttonTypes.button} onClick={onClose}>
              {btn1.text}
            </Button>
          )}
          {btn2 && (
            <Button
              type={
                btn2.method ? cfg.buttonTypes.button : cfg.buttonTypes.submit
              }
              onClick={onConfirm}>
              {btn2.text}
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

ConfirmationModal.propTypes = {
  className: PropTypes.string,
  btn1: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      text: PropTypes.string,
      method: PropTypes.func,
    }),
  ]),
  btn2: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.shape({
      text: PropTypes.string,
      method: PropTypes.func,
    }),
  ]),
  onConfirm: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};

export default ConfirmationModal;
