import React, { useContext } from 'react';

import * as PropTypes from 'prop-types';

import { ThemeContext } from '../common/ThemeProvider';

function ThemeWrapper({ children }) {
  const { mode } = useContext(ThemeContext);

  return <div className={`theme-${mode}`}>{children}</div>;
}

ThemeWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

export default ThemeWrapper;
