import React from 'react';

import renderer from 'react-test-renderer';

import Timer from '../../features/timer/pages/timer-page';

describe('Snapshots', () => {
  test('Timer', () => {
    const component = renderer.create(<Timer />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
