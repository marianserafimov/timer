import Home from '../features/home/pages/home';
import Timer from '../features/timer/pages/timer-page';
import Weather from '../features/weather/pages/weather-page';
import MainLayout from '../layouts/MainLayout';

const routesConfig = {
  timerPage: {
    path: '/timer',
    component: Timer,
    layout: MainLayout,
  },
  weatherPage: {
    path: '/weather',
    component: Weather,
    layout: MainLayout,
  },
  homePage: {
    path: '/',
    component: Home,
    layout: MainLayout,
  },
};

export default routesConfig;
