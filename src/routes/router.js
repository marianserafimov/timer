import React from 'react';

import { Switch } from 'react-router-dom';
import { map, pipe, values } from 'ramda';
import { v4 as uuidv4 } from 'uuid';

import AppRoute from './app-route';
import routesConfig from './routes-config';

const mapRoutes = pipe(
  map(({ path, component, layout }) => (
    <AppRoute
      key={uuidv4()}
      path={path}
      component={component}
      layout={layout}
    />
  )),
  values,
);

const Router = () => <Switch>{mapRoutes(routesConfig)}</Switch>;

export default Router;
