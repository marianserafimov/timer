export default {
  texts: {
    logo: 'Timer',
    btn_close_confirmation_modal: 'No',
    btn_confirm_confirmation_modal: 'Yes',
    reset_confirm_text: 'Are you sure you would like to reset the timer?',
    increment_confirm_text:
      'Are you sure you would like to determine the step by which the timer will increment - Value $[value] means the timer will increase $[value] times faster.?',
    actionButtons: {
      start: 'Start',
      stop: 'Stop',
      reset: 'Reset',
      increment: 'Increment',
    },
    formPlaceolders: {
      hours: 'Hours:',
      minutes: 'Minutes:',
      seconds: 'Seconds:',
    },
    weather: {
      tableRows: {
        date: 'Date',
        degrees: 'Degrees',
        status: 'Status',
        icon: 'Icon',
      },
    },
    themes: {
      defaultTheme: 'light',
      light: 'light',
      dark: 'dark',
    },
  },

  defaultValues: {
    timer: {
      count: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
      incrementor: 1,
      isTimerPlaying: false,
    },
    weather: {
      town: '',
    },
  },

  cssColorVariables: [
    'primary',
    'secondary',
    'tertiary',
    'quaternary',
    'penitentiary',
  ],

  types: {
    string: 'string',
    number: 'number',
    object: 'object',
    boolean: 'boolean',
    symbol: 'symbol',
    function: 'function',
  },

  buttonTypes: {
    submit: 'submit',
    button: 'button',
    reset: 'reset',
  },

  apiKeys: {
    weather: '99b2352079954b658939b583cd1cbe26',
  },
};
