export default class WeatherService {
  static prepareWeatherItem(weather) {
    return {
      date: weather.dt_txt,
      icon: weather.weather[0].icon,
      status: weather.weather[0].description,
      degrees: (weather.main.temp - 273).toFixed(2),
    };
  }
}
