import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';

import Router from './routes/router';
import configureStore from './store/store';
import { api } from './api-services';
import ThemeProvider from './components/common/ThemeProvider';
import ThemeWrapper from './components/wrappers/ThemeWrapper';

import './styles/app.scss';
import 'react-toastify/dist/ReactToastify.css';

// Store
const apis = {
  ajaxApi: api(),
};
const store = configureStore(apis);

toast.configure({
  autoClose: 8000,
  draggable: false,
});

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider>
        <ThemeWrapper>
          <div className="App">
            <BrowserRouter>
              <Router />
            </BrowserRouter>
          </div>
        </ThemeWrapper>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
