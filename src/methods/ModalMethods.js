module.exports = {
  openConfirmationModal() {
    this.setState({ modal: true });
  },

  closeConfirmationModal() {
    this.setState({ modal: false });
  },
};
