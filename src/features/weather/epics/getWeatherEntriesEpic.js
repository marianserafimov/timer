import { ofType } from 'redux-observable';
import { mergeMap } from 'rxjs/operators';
import { merge, of } from 'rxjs';

import { GET_WEATHER } from '../types';
import {
  getWeatherEntriesPending,
  getWeatherEntriesFulfilled,
  getWeatherEntriesRejected,
} from '../actions';
import {
  openPreloaderAction,
  closePreloaderAction,
  sendApiRequest,
  notifyWarning,
  notifyError,
} from '../../../store/actions';

import generateError from '../../../utilities/generateError';
import { WEATHER_MESSAGES } from '../constants/weatherMessages';

const createPayload = (payload) => ({
  method: 'get',
  url: 'http://api.openweathermap.org/data/2.5/forecast',
  onSuccess: [
    closePreloaderAction,
    (response) =>
      response.data.length === 0
        ? notifyWarning(WEATHER_MESSAGES.NOT_FOUND)
        : getWeatherEntriesFulfilled(response),
  ],
  onError: [
    getWeatherEntriesRejected,
    closePreloaderAction,
    (error) => notifyError(generateError(error)),
  ],
  data: payload,
});

const meta = {
  api: 'ajaxApi',
};

const getWeatherEntriesEpic$ = (action$) =>
  action$.pipe(
    ofType(GET_WEATHER.DEFAULT),
    mergeMap(({ payload }) =>
      merge(
        of(getWeatherEntriesPending(), openPreloaderAction()),
        of(sendApiRequest(createPayload(payload), meta)),
      ),
    ),
  );

export default getWeatherEntriesEpic$;
