import { createReducer } from 'redux-create-reducer';

import { GET_WEATHER, CLEAR_WEATHER } from '../types';

const initialState = {
  town: {},
  entries: [],
  isLoading: false,
};

const weatherReducer = createReducer(initialState, {
  [GET_WEATHER.DEFAULT]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [GET_WEATHER.PENDING]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [GET_WEATHER.REJECTED]: (state) => ({
    ...state,
    isLoading: false,
  }),
  [GET_WEATHER.FULFILLED]: (state, { payload }) => ({
    entries: payload.data.list,
    town: payload.data.city,
    isLoading: false,
  }),
  [CLEAR_WEATHER]: () => initialState,
});

export default weatherReducer;
