import React, { useEffect, useState } from 'react';

import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import { weatherEntriesSelector, weatherTownSelector } from '../../selectors';
import { getWeatherEntries, clearWeather } from '../../actions';
import TownSelectorForm from '../../components/town-selector-form';
import WeatherTable from '../../components/weather-table';
import TownInfo from '../../components/town-info';
import cfg from '../../../../cfg';

function Weather() {
  const dispatch = useDispatch();
  const [town, setTown] = useState(cfg.defaultValues.weather.town);

  const weatherEntries = useSelector(weatherEntriesSelector);
  const weatherTown = useSelector(weatherTownSelector);

  const changeTown = (data) => {
    if (town !== data.town) {
      setTown(data.town);
      dispatch(getWeatherEntries({ q: data.town, APPID: cfg.apiKeys.weather }));
      dispatch(clearWeather());
    }
  }

  useEffect(() => () => dispatch(clearWeather()), []);

  return (
    <Container className="weather">
      <TownSelectorForm changeTown={changeTown} />
      {weatherTown && Object.keys(weatherTown).length !== 0 && (
        <TownInfo town={weatherTown} />
      )}
      {weatherEntries && Object.keys(weatherEntries).length !== 0 && (
        <WeatherTable entries={weatherEntries} />
      )}
    </Container>
  );
}

export default Weather;
