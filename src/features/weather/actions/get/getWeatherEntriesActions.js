import { GET_WEATHER } from '../../types';
import createAsyncActions from '../../../../utilities/createAsyncActions';

export const {
  getWeatherEntries,
  getWeatherEntriesPending,
  getWeatherEntriesFulfilled,
  getWeatherEntriesRejected,
} = createAsyncActions(GET_WEATHER, 'getWeatherEntries');
