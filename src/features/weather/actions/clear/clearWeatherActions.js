import { createAction } from 'redux-actions';

import { CLEAR_WEATHER } from '../../types';

export const clearWeather = createAction(CLEAR_WEATHER);
