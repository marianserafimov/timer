import { path } from 'ramda';

export const weatherEntriesSelector = path(['weather', 'entries']);
export const weatherTownSelector = path(['weather', 'town']);
