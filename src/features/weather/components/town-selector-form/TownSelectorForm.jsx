import React from 'react';

import * as PropTypes from 'prop-types';

import { Formik, Form, Field } from 'formik';
import { ReactComponent as SearchSvg } from '../../../../assets/images/icons/search-icon.svg';
import cfg from '../../../../cfg';

function CitySelectorForm({ changeTown }) {
  return (
    <Formik
      initialValues={{
        town: cfg.defaultValues.weather.town,
      }}
      onSubmit={(values) => changeTown(values)}>
      {() => (
        <Form>
          <Field id="town" name="town" placeholder="Type town..." />
          <button type="submit">
            <SearchSvg />
          </button>
        </Form>
      )}
    </Formik>
  );
}

CitySelectorForm.propTypes = {
  changeTown: PropTypes.func.isRequired,
};

export default CitySelectorForm;
