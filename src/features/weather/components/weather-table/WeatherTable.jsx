import React from 'react';

import * as PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { Table } from 'reactstrap';

import WeatherTableItem from './weather-table-item';
import cfg from '../../../../cfg';

function WeatherTable({ entries }) {
  const { date, degrees, status, icon } = cfg.texts.weather.tableRows;
  return (
    <Table>
      <thead>
        <tr>
          <th>{date}</th>
          <th>{degrees}</th>
          <th>{status}</th>
          <th>{icon}</th>
        </tr>
      </thead>
      {entries.map((entry) => (
        <WeatherTableItem key={uuidv4()} weather={entry} />
      ))}
    </Table>
  );
}

WeatherTable.propTypes = {
  entries: PropTypes.array.isRequired,
};

export default WeatherTable;
