import React from 'react';

import * as PropTypes from 'prop-types';

import WeatherService from '../../../../../services/WeatherService';

function WeatherTableItem({ weather }) {
  const { date, degrees, status, icon } = WeatherService.prepareWeatherItem(
    weather,
  );

  return (
    <tbody>
      <tr>
        <th>{date}</th>
        <th>{degrees}</th>
        <th>{status}</th>
        <th>
          <img
            alt={`${status}_${icon}`}
            src={require(`../../../../../assets/images/weather-icons/${icon}.png`).default}
          />
        </th>
      </tr>
    </tbody>
  );
}

WeatherTableItem.propTypes = {
  weather: PropTypes.object.isRequired,
};

export default WeatherTableItem;
