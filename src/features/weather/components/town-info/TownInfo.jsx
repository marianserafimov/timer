import React from 'react';

import * as PropTypes from 'prop-types';

function TownInfo({ town }) {
  const { name, country, population } = town;

  return (
    <div className="town-info">
      <>
        <h2>{`${name}, ${country}`}</h2>
        <h4>
          Population:
          {population}
        </h4>
      </>
    </div>
  );
}

TownInfo.propTypes = {
  town: PropTypes.shape({
    name: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    population: PropTypes.number.isRequired,
  }),
};

export default TownInfo;
