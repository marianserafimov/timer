import createAsyncActionType from '../../../utilities/createAsyncActionType';

// GET
export const GET_WEATHER = createAsyncActionType('GET_WEATHER');

// CLEAR
export const CLEAR_WEATHER = 'CLEAR_WEATHER';
