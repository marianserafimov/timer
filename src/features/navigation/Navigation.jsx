import React from 'react';
import { Link } from 'react-router-dom';

import routesConfig from '../../routes/routes-config';

const Navigation = () => (
  <nav className="navbar navbar-expand-md navbar-light">
    <ul className="navbar-nav">
      <li className="nav-item">
        <Link className="nav-link" to={routesConfig.homePage.path}>
          Home
        </Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to={routesConfig.timerPage.path}>
          Timer
        </Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to={routesConfig.weatherPage.path}>
          Weather
        </Link>
      </li>
    </ul>
  </nav>
);

export default Navigation;
