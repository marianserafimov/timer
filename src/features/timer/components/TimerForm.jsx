import React from 'react';

import * as PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';

import cfg from '../../../cfg';

function TimerForm({
  handleHoursChange,
  handleMinutesChange,
  handleSecondsChange,
  time,
  isTimerPlaying,
}) {
  const { hours, minutes, seconds } = time;
  const { formPlaceolders } = cfg.texts;

  return (
    <Row>
      <Col className="text-center">
        <h3>{formPlaceolders.hours}</h3>
        <Form.Group>
          <Form.Control
            onChange={(e) => handleHoursChange(e.target.value)}
            value={hours}
            type="number"
            disabled={isTimerPlaying}
          />
        </Form.Group>
      </Col>
      <Col className="text-center">
        <h3>{formPlaceolders.minutes}</h3>
        <Form.Group>
          <Form.Control
            onChange={(e) => handleMinutesChange(e.target.value)}
            value={minutes}
            type="number"
            disabled={isTimerPlaying}
          />
        </Form.Group>
      </Col>
      <Col className="text-center">
        <h3>{formPlaceolders.seconds}</h3>
        <Form.Group>
          <Form.Control
            onChange={(e) => handleSecondsChange(e.target.value)}
            value={seconds}
            type="number"
            disabled={isTimerPlaying}
          />
        </Form.Group>
      </Col>
    </Row>
  );
}

TimerForm.propTypes = {
  handleHoursChange: PropTypes.func.isRequired,
  handleMinutesChange: PropTypes.func.isRequired,
  handleSecondsChange: PropTypes.func.isRequired,
  time: PropTypes.shape({
    hours: PropTypes.number.isRequired,
    minutes: PropTypes.number.isRequired,
    seconds: PropTypes.number.isRequired,
  }),
  isTimerPlaying: PropTypes.bool.isRequired,
};

export default TimerForm;
