import React, { useState } from 'react';

import * as PropTypes from 'prop-types';
import { Button, Form, Row, Col } from 'react-bootstrap';

import ConfirmationModal from '../../../components/modals/confirmation-modal';
import cfg from '../../../cfg';

function Incrementor({ setIncrementor }) {
  const [modal, setModal] = useState(false);
  const [incrementVal, setIncrementVal] = useState(1);

  return (
    <Row className="incrementor">
      <Col lg={3}>
        <Form.Group>
          <Form.Control
            onChange={(e) => setIncrementVal(e.target.value)}
            value={incrementVal}
            type="number"
          />
        </Form.Group>
        <Button variant="secondary" onClick={() => setModal(true)}>
          {cfg.texts.actionButtons.increment}
        </Button>

        {modal && (
          <ConfirmationModal
            className="confirmation-modal"
            onClose={() => setModal(false)}
            // eslint-disable-next-line radix
            onConfirm={() => setIncrementor(parseInt(incrementVal))}
            btn1={{
              text: cfg.texts.btn_close_confirmation_modal,
            }}
            btn2={{
              text: cfg.texts.btn_confirm_confirmation_modal,
            }}>
            <h3>
              {cfg.texts.increment_confirm_text.replaceAll(
                '$[value]',
                String(incrementVal),
              )}
            </h3>
          </ConfirmationModal>
        )}
      </Col>
    </Row>
  );
}

Incrementor.propTypes = {
  setIncrementor: PropTypes.func.isRequired,
};

export default Incrementor;
