import React, { useState } from 'react';

import * as PropTypes from 'prop-types';
import { Button, Row, Col } from 'react-bootstrap';

import ConfirmationModal from '../../../components/modals/confirmation-modal';
import cfg from '../../../cfg';

function ActionButtons({ startTimer, stopTimer, resetTimer }) {
  const [modal, setModal] = useState(false);

  return (
    <Row className="action-buttons">
      <Col>
        <Button variant="primary" onClick={startTimer}>
          {cfg.texts.actionButtons.start}
        </Button>
        <Button variant="danger" onClick={stopTimer}>
          {cfg.texts.actionButtons.stop}
        </Button>
        <Button variant="warning" onClick={() => setModal(true)}>
          {cfg.texts.actionButtons.reset}
        </Button>

        {modal && (
          <ConfirmationModal
            className="confirmation-modal"
            onClose={() => setModal(false)}
            onConfirm={resetTimer}
            btn1={{
              text: cfg.texts.btn_close_confirmation_modal,
            }}
            btn2={{
              text: cfg.texts.btn_confirm_confirmation_modal,
            }}>
            <h3>{cfg.texts.reset_confirm_text}</h3>
          </ConfirmationModal>
        )}
      </Col>
    </Row>
  );
}

ActionButtons.propTypes = {
  startTimer: PropTypes.func.isRequired,
  stopTimer: PropTypes.func.isRequired,
  resetTimer: PropTypes.func.isRequired,
};

export default ActionButtons;
