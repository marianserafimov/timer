/* eslint-disable radix */
import React, { useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import ActionButtons from '../../components/ActionButtons';
import Incrementor from '../../components/Incrementor';
import cfg from '../../../../cfg';
import TimerForm from '../../components/TimerForm';

function Timer() {
  const [count, setCount] = useState(cfg.defaultValues.timer.count);

  const [hours, setHours] = useState(cfg.defaultValues.timer.hours);
  const [minutes, setMinutes] = useState(cfg.defaultValues.timer.minutes);
  const [seconds, setSeconds] = useState(cfg.defaultValues.timer.seconds);

  const [incrementor, setIncrementor] = useState(
    cfg.defaultValues.timer.incrementor,
  );

  const [isTimerPlaying, setIsTimerPlaying] = useState(
    cfg.defaultValues.timer.isTimerPlaying,
  );

  useEffect(() => {
    let interval = null;

    if (isTimerPlaying) {
      interval = setInterval(() => {
        // eslint-disable-next-line no-shadow
        setCount((count) => count + 1);
      }, 1000 / incrementor);
    } else if (!isTimerPlaying && seconds !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isTimerPlaying, incrementor]);

  useEffect(() => {
    setSeconds(count % 60);
    setMinutes(Math.floor((count / 60) % 60));
    setHours(Math.floor(count / 3600));
  }, [count]);

  const startTimer = () => {
    setCount(hours * 3600 + minutes * 60 + seconds);
    setIsTimerPlaying(true);
  }

  const stopTimer = () => {
    setIsTimerPlaying(false);
  }

  const resetTimer = () => {
    setCount(0);
    setIncrementor(1);
  }

  const handleHoursChange = (val) => {
    setHours(parseInt(val));
  }

  const handleMinutesChange = (val) => {
    if (val > -1 && val < 60) {
      setMinutes(parseInt(val));
    }
  }

  const handleSecondsChange = (val) => {
    if (val > -1 && val < 60) {
      setSeconds(parseInt(val));
    }
  }

  return (
    <Container className="timer">
      <TimerForm
        handleHoursChange={handleHoursChange}
        handleMinutesChange={handleMinutesChange}
        handleSecondsChange={handleSecondsChange}
        time={{ hours, minutes, seconds }}
        isTimerPlaying={isTimerPlaying}
      />
      <ActionButtons
        startTimer={startTimer}
        stopTimer={stopTimer}
        resetTimer={resetTimer}
      />
      <Incrementor setIncrementor={setIncrementor} />
    </Container>
  );
}

export default Timer;
